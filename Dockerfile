FROM node:14

WORKDIR /app

COPY .git .git
COPY .gitmodules .gitmodules
RUN git submodule update --init

WORKDIR /app/front

RUN npm ci
RUN npm run build -- --prod
RUN mv dist/test ../public

WORKDIR /app/back

RUN npm ci
RUN npm run compile
RUN mv dist ../dist
RUN npm prune --production
RUN mv node_modules ../node_modules
RUN mv package.json ../package.json

WORKDIR /app

RUN rm -r front back .git .gitmodules

CMD npm start